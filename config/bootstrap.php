<?php
// In ContactManager plugin bootstrap.php
use Cake\Event\EventManager;
use Cakesol\Config\Middleware\ConfigMiddleware;

EventManager::instance()->on(
    'Server.buildMiddleware',
    function ($event, $middlewareQueue) {
        $middlewareQueue->add(new ConfigMiddleware());
    });