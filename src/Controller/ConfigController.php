<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace Cakesol\Config\Controller;

use Cakesol\Config\Model\Table\ConfigsTable;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class TestController extends AppController
{

    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize()
    {
        $this->loadModel('Cakesol/Config.Configs');
    }

    public function index()
    {
        $list = $this->Configs
            ->find('children', ['for' => 1])
            ->find('threaded')
            ->toArray();
        
        foreach ($list as $row) {
            debug($row);
        }
        $list = $this->Configs->find('threaded', array(
            'fields' => array('key', 'value'),
            'order' => array('lft ASC') // or array('id ASC')
        ));
        foreach ($list as $row) {
            debug($row);
        }
        die('asd');
    }
}
