<?php
namespace Cakesol\Config\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Cake\Core\Configure;
use Cake\Datasource\ModelAwareTrait;
use Cakesol\Config\Model\Table\ConfigsTable;

/**
 * Config middleware
 */
class ConfigMiddleware
{

    use ModelAwareTrait;

    /**
     * Invoke method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Message\ResponseInterface $response The response.
     * @param callable $next Callback to invoke the next middleware.
     * @return \Psr\Http\Message\ResponseInterface A response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        $configTable = $this->loadModel('Cakesol/Config.Configs');
        $list = $configTable
            ->find('threaded');
        $results = [];
        $configs = $configTable->toArray($results, $list);

        foreach ($configs as $configName => $data) {
            Configure::write($configName, $data);
        }
        return $next($request, $response);
    }
}
